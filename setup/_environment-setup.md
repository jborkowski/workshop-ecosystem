# Environment Access

## Environment Setup

    $ apt-get install --yes git vim nmap htop wget curl unzip

## Locale

    $ echo 'LANG="en_US.UTF-8"' >> /etc/default/locale
    $ echo 'LC_ALL="en_US.UTF-8"' >> /etc/default/locale
    $ echo 'LANG="en_US.UTF-8"' >> /etc/default/locale
    $ locale-gen en_US.UTF-8
    $ dpkg-reconfigure locales
