# Running application inside Docker container and docker-compose

## English

- Download repository from https://github.com/spring-guides/gs-spring-boot-docker.git
- Build project using `gradle`
- Run application using `Docker`
- Create `docker-compose.yaml` wtih container description

## Polish

- Ściągnij repozytorium https://github.com/spring-guides/gs-spring-boot-docker.git
- Zbuduj projekt za pomocą `gradle`
- Uruchom aplikację wykorzystując `Docker`
- Użyj pliku `docker-compose.yaml` do opisu środowiska kontenera
