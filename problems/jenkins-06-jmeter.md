# Jenkins and JMeter

## English

- Run load test for main page of application run on you computer (ie. `SonarQube` if you have solved the previous problems)
- Load test should be in `xml` file and run without GUI

## Polish

- Przeprowadź test wydajnościowy głównej strony aplikacji uruchomionej na Twoim komputerze (np. `SonarQube` jeżeli wykonałeś poprzednie ćwiczenie)
- Test wydajnościowy powinien zapisany w `xml` oraz uruchamiany bez wykorzystania GUI
