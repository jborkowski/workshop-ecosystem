# Pull Requests builds

## English

- Configure plan to build `GIT Flow` branches
    - `Pull Requests`
    - `feature`
    - `bugfix`
    - `master`

## Polish

- Skonfiguruj plan by budował gałęzie `GIT Flow`
    - `Pull Requests`
    - `feature`
    - `bugfix`
    - `master`
