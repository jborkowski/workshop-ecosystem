# Jenkins Docker Plugin

## English

- Install `Docker Plugin` in `Jenkins`
- Configure job to run inside container
- Job has to provision the environment
- Job has to build the project inside container
- Job has to destroy container after build

## Polish

- Zainstaluj `Docker Plugin` w `Jenkins`
- Skonfiguruj zadanie aby uruchamiało kontener
- Zadanie ma provisionować konfigurację wewnątrz kontenera
- Zadanie ma uruchamiać build wewnątrz kontenera
- Zadanie ma niszczyć kontener po buildze
