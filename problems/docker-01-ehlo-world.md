# Docker Ehlo World

## English

- Install Docker
- What is the difference between `Docker` and `Vagrant`?
- Can Docker be run on `Windows`?
- Can Docker be run on `OS X`?
- Print `Ehlo World!` from inside of `Docker` container
- Print list of running `Docker` containers

## Polish

- Zainstaluj `Docker`
- Czym różni się `Docker` od `Vagrant`?
- Czy Docker może być na `Windows`?
- Czy Docker może być na `OS X`?
- Wyświetl `Ehlo World!` z wnętrza kontenera `Docker`
- Wyświetl listę działających kontenerów `Docker`
