# Amazon AWS

## English

- Download repository https://github.com/heroku/java-sample.git
- Create an `Amazon AWS` account
- From the dashboard run the `Frie Tier` `Ubuntu LTS` AMI
- In the `Security Groups` configuration allow connections from ports
    - 80
    - 443
    - 8080
    - 9000
    - 8081

## Polish

- Ściągnij repozytorium https://github.com/heroku/java-sample.git
- Załóż konto na `Amazon AWS`
- W panelu sterowania uruchom maszynę z poziomu `Free Tier` z `Ubuntu LTS` AMI
- W konfiguracji sieciowej maszyny ustaw możliwość połączenia z maszyną na portach:
    - 80
    - 443
    - 8080
    - 9000
    - 8081
