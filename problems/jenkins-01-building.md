# Jenkins installation, configuration and builds

## English

- Install `Jenkins` using `DEB` packages by `apt-get`
- Alternatively you might want to use `Docker` or `Puppet` manifests
- Download repository https://github.com/SonarSource/sonar-examples.git
- Start building projects in `sonar-examples/projects/languages/java`
    - `ut` - unit tests
    - `it` - integration tests

## Polish

- Zainstaluj `Jenkins` za pomocą paczek `DEB` przez `apt-get`
- Alternatywnie możesz użyć `Docker` albo manifestów `Puppeta`
- Zaciągnij repozytorium https://github.com/SonarSource/sonar-examples.git
- Zacznij budować różne projekty `sonar-examples/projects/languages/java`
    - `ut` - unit tests
    - `it` - integration tests
