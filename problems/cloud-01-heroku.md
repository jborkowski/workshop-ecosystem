# Heroku

## English

- Download repository https://github.com/heroku/java-sample.git
- Create `Heroku` account
- Create a new application
- Add `heroku` repository URL to your local `GIT` repository remotes
- Push application to the `Heroku`

## Polish

- Ściągnij repozytorium https://github.com/heroku/java-sample.git
- Załóż konto na `Heroku`
- Stwórz nową aplikację
- Dodaj remote `Heroku` do lokalnego repozytorium `GIT`
- Uruchom aplikację na `Heroku`
