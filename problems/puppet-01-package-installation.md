# Puppet package installation

## English

- Create manifest in `/etc/puppet/manifests/packages.pp`
- `Puppet` should install those packages:
    - `nmap`
    - `htop`
    - `git`
- Make sure that `apt-get update` command is run before

## Polish

- Manifest do tego zadania zapisz w pliku `/etc/puppet/manifests/packages.pp`
- Zainstaluj następujące pakiety za pomocą `Puppet`:
    - `nmap`
    - `htop`
    - `git`
- Upewnij się by `Puppet` wykonał polecenie `apt-get update` na początku
